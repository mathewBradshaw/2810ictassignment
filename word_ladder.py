import re

#Returns a list of all words from dictionary that are the same length as target.
def same(item, target):
  return len([c for (c, t) in zip(item, target) if c == t])

#Returns a list words that follow the pattern given to the build function that are not in the ignore list or seen list
def build(pattern, words, seen, list, ignore):
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and
                    word not in list and word not in ignore]

#Builds a list of candidate words using the build function and then sorts them by matching letters to the target word
#returns a boolean value based on wheather or not a path from start to target is possible
def find(word, words, seen, target, path, ignore, shortCheck):
  list = []
  for i in range(len(word)):
    list += build(word[:i] + "." + word[i + 1:], words, seen, list, ignore)
  if len(list) == 0:
    return False
  list = sorted([(same(w, target), w) for w in list], reverse = True)
  for (match, item) in list:
    if match >= len(target) - 1:
      if match == len(target) - 1:
        path.append(item)
      return True
    seen[item] = True
  for (match, item) in list:
    #shortCheck if shortest path was requested
    if shortest == 'y':
        if shortCheck <= match:
            shortCheck = match
            path.append(item)
            if find(item, words, seen, target, path, ignore, shortCheck):
                return True
            path.pop()
    #else original code
    else:
        path.append(item)
        if find(item, words, seen, target, path, ignore, shortCheck):
          return True
        path.pop()
#Uses try/except loop to prompt user for the dictionary name
dictFile = 0
while dictFile==0:
    try:
      fname = input("Enter dictionary name: ")
      file = open(fname)
      lines = file.readlines()
      dictFile = 1
    #re-prompt if file name was invalid/does not exist
    except FileNotFoundError:
        print('Error: failed to open dictionary, please try again.')
startEval = 0
targetEval = 0
#Use while loop to promp user for the start word, checks if it is alphanumeric, otherwise re-prompt
while startEval == 0:
    start = input("Enter start word:")
    if start.isalpha() == False:
        print('Error: Start word must only consist of letters.')
    else:
        startEval = 1
words = []
for line in lines:
    word = line.rstrip()
    if len(word) == len(start):
        words.append(word)
#Use while loop to promp user for the target word, checks if it is alphanumeric and same length as start
# #otherwise re-prompt
while targetEval == 0:
    target = input("Enter target word:")
    if target.isalpha() == False:
        print('Error: Target word must only consist of letters.')
    elif len(start) == len(target):
        targetEval = 1
    else:
        print('Start and target words must be the same length')
ignore = []
igWord = ''
#prompt user for ignore words, enter '0' to when finished
#checks if ignore words are same length as start and alphanumeric
while igWord != '0':
    igWord = input('Enter any words you would like to ignore, enter "0" to continue: ')
    if igWord.isalpha == False:
        print('Input must only consist of letters.')
    elif len(igWord) == len(start):
        ignore.append(igWord)
    elif igWord == '0':
        pass
    else:
        print('Input must be the same length as start/target word.')
print(ignore)
shortest = ''
#asks user if they would like the shortest rout to target (otherwise greedy method)
while shortest != 'y' and shortest != 'n':
    shortest = input('Would you like to find the shortest path? (y/n): ')
#initialise variables
count = 0
shortCheck = 0
path = [start]
seen = {start : True}
#call find and run program
if find(start, words, seen, target, path, ignore, shortCheck):
  path.append(target)
  print(len(path) - 1, path)
else:
  print("No path found")

