import unittest
import word_ladder as wordLad

def test_setup(startT, targetT, shortT, ignoreT):
    wordLad.start = startT
    wordLad.target = targetT
    wordLad.shortest = shortT
    wordLad.path = [startT]
    wordLad.seen = {startT: True}
    wordLad.ignore = ignoreT
    wordsT = []
    file = open('dictionary.txt')
    lines = file.readlines()
    for line in lines:
        word = line.rstrip()
        if len(word) == len(startT):
            wordsT.append(word)
    if wordLad.find(wordLad.start, wordLad.words, wordLad.seen, wordLad.target, wordLad.path, wordLad.ignore, 0):
        wordLad.path.append(wordLad.target)
        print(len(wordLad.path) - 1, wordLad.path)
        return len(wordLad.path)
    else:
        print("No path found")
class testCases(unittest.TestCase):

    def test_dict(self):
        self.assertEqual(wordLad.fname,'dictionary.txt')

    def test_dict1(self):
        wordLad.fname = 'dictionary.tx'
        self.assertRaises(FileNotFoundError)

    def test_dict2(self):
        wordLad.fname = ''
        self.assertRaises(FileNotFoundError)

    def test_dict3(self):
        wordLad.fname = 'badDict.txt'
        self.assertRaises(FileNotFoundError)

    def test_leadGoldT(self):
        pathlen = test_setup('lead','gold', 'y', [])
        self.assertEqual(pathlen-1, 3)

    def test_leadGoldIgnore(self):
        pathlen = test_setup('lead','gold', 'y', ['goad'])
        self.assertEqual(pathlen-1, 4)

    def test_hideSeek(self):
        pathlen = test_setup('hide', 'seek', 'y', [])
        self.assertEqual(pathlen - 1, 6)

    def test_hideSeekLong(self):
        pathlen = test_setup('hide', 'seek', 'n', [])
        self.assertEqual(pathlen - 1, 28)

if __name__ == '__main__':
   unittest.main()